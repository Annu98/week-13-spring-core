package com.glearning.spring.core;

public class UberGoDriver implements UberGo {
	
	public void registerWithUber() {
		System.out.println("Uberdriver - Registering with Uber");
	}
	
	public void trip(String source, String destination) {
		System.out.println("travelling from "+source+ " to "+ destination+ " by UberGo!!");
	}
	
	public void deregisterWithUber() {
		System.out.println("Uberdriver - De Registering with Uber");
	}

}
